# React-Redux Form

React-Redux Form consuming data from a GraphQL Database

## Including

- React
- Redux
- Bootstrap
- Unit-Tests: Jest & Enzyme

## Installing Dependencies

```
yarn/npm install
```

## Running App

```
yarn/npm start
```

## Running the tests

```
yarn/npm run test:ci
```







