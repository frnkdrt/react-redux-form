import React from 'react'
import { shallow, mount, render } from 'enzyme'
import { shallowToJson, toJSON } from 'enzyme-to-json'
import 'jest-styled-components'
import ShortTextField from '../../molecules/ShortTextField'

describe('ShortTextField view', () => {
	let wrapper
	beforeEach(() => {
		wrapper = shallow(<ShortTextField />)
	})

	it('should render withouth thowring an error', () => {
		expect(shallowToJson(wrapper)).toMatchSnapshot()
	})

	it('should contain a label, with default label inside', () => {
		const elementToMatch = <label>default_label</label>

		expect(wrapper.containsMatchingElement(
			elementToMatch
		)).toEqual(true)
	})

	it('should contain an input field, with type="text", className="form-control" ', () => {
		const elementToMatch = 
			<input 
				className="form-control"
				type="text"
			/>

		expect(wrapper.containsMatchingElement(
			elementToMatch
		)).toEqual(true)
	})

	it('should have the correct default id', () => {
		expect(wrapper.find('div').props().id).toEqual('default_id')
	})

	it('should have the corret default label', () => {
		expect(wrapper.find('label').text()).toEqual('default_label')
	})

	it('should have the correct default value', () => {
		expect(wrapper.find('input').props().value).toEqual('')
	})
})

describe('ShortTextField behaviour', () => {
	let wrapper;
	const mockOnChangeFieldValue = jest.fn()
	beforeEach(() => {
		wrapper = shallow(<ShortTextField onChangeFieldValue={mockOnChangeFieldValue} />)
	})

	it('should change id, __typename, value in render when receiving props', () => {
		wrapper.setProps({ id: 'your_name', value : "writ.", label: "default_label_02" })

		expect(wrapper.find('div').props().id).toEqual('your_name')
		expect(wrapper.find('div').find('input').props().value).toEqual('writ.')
		expect(wrapper.find('label').text()).toEqual('default_label_02')
	})


	it('should call `onFieldChange` function on input change', () => {
		wrapper.find('input').simulate( 
			'change',
			{ target : { value : 'Writ...'}}
		)

		wrapper.find('input').simulate(
			'change',
			{ target : { value : 'Writting...'}}
		)

		expect(mockOnChangeFieldValue.mock.calls.length).toBe(2)
	})

	it('should call `onChangeFieldValue` with correct `id`, and `value`', () => {
		wrapper.find('input').simulate(
			'change',
			{ target : { value: 'Writ...'}}
		)

		expect(mockOnChangeFieldValue.mock.calls[2]).toEqual(['default_id', 'Writ...'])
	})
})