import React from 'react'
import { shallow, mount, render } from 'enzyme'
import { shallowToJson, toJSON } from 'enzyme-to-json'
import 'jest-styled-components'
import DateField from '../../molecules/DateField'

describe('DateField view', () => {
	let wrapper
	let renderedInput
	beforeEach(() => {
		wrapper = shallow(<DateField />)
		// TODO: check value prop passed in parameter, maybe not necessary when using redux
		renderedInput = shallow(wrapper.props().renderInput({ value: '01/01/0001'}, () => {}, () => {} ))
	})

	it('should render withouth thowring an error', () => {
		expect(shallowToJson(wrapper)).toMatchSnapshot()
	})

	it('should contain a input, with className="form-control", type="text", placeholder, value', () => {
		const elementToMatch = (
			<input 
				id="default_id"
				className="form-control"
				type="text"
				placeholder={`99/99/9999`}
				value={`99/99/9999`}
			/>
		)

		expect(renderedInput.containsMatchingElement(
			elementToMatch
		)).toEqual(true)
	})

	it('should have the correct default id', () => {

		expect(renderedInput.find('input').props().id).toEqual('default_id')
	})

	it('should have the corret default label', () => {
		expect(renderedInput.find('label').text()).toEqual('default_label')
	})

	it('should have the correct default value', () => {
		expect(renderedInput.find('input').props().value).toEqual('99/99/9999')
	})
})

describe('DateField behaviour', () => {
	let wrapper
	let renderedInput
	const mockOnChangeFieldValue = jest.fn()
	beforeEach(() => {
		wrapper = shallow(<DateField onChangeFieldValue={mockOnChangeFieldValue} />)
		renderedInput = shallow(wrapper.props().renderInput({ id: 'default_id', value: '01/01/0001', label: "default_label_02"}, () => {}, () => {} ))
	})

	it('should change id, __typename, value in render when receiving props', () => {

		expect(renderedInput.find('input').props().id).toEqual('default_id')
		expect(renderedInput.find('div').find('input').props().value).toEqual('99/99/9999')
		expect(renderedInput.find('label').text()).toEqual('default_label')
	})

	// TODO test onChange Datetime event
})