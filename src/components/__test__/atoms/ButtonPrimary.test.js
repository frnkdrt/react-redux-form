import React from 'react'
import { shallow, mount, render } from 'enzyme'
import { shallowToJson, toJSON } from 'enzyme-to-json'
import 'jest-styled-components'
import styled from 'styled-components'
import ButtonPrimary from '../../atoms/ButtonPrimary'

import renderer from 'react-test-renderer'

describe('ButtonPrimary', () => {
	it('should render withouth thowring an error', () => {
		
		const wrapper = shallow(<ButtonPrimary />)

		expect(shallowToJson(wrapper)).toMatchSnapshot()
	})

	it('should render correctly', () => {
		const wrapper = renderer.create(<ButtonPrimary />).toJSON()

		expect(wrapper).toMatchSnapshot()
	})
})