import React, { Component } from 'react'
import { connect } from 'react-redux'
import { findFieldInState } from '../../selectors/RecruitmentSurveySelectors'
import * as actions from '../../actions/RecruitmentSurveyActions'
import { validate } from '../../utils/validation'

export default class ChecklistVerticalField extends Component {

	constructor (props) {
		super(props)
		this.state = {
			id: "default_value",
			__typename: "default_typename",
			label: "default_value",
			options: ["one", "two"],
			value: [],
			validationState : {
				status: '',
				message: []
			}
		}
		this.validateField = this.validateField.bind(this)
		this.handleChange = this.handleChange.bind(this)
	}

	validateField (value) {
		const errors = validate([
			{ property: "Additional Experience", rule_name: "isRequired" }
		])({ "Additional Experience": value })

		return {
			status: errors.length > 0 ? 'is-valid' : 'is-invalid',
			message : errors
		}
	}

	handleChange = (ev) => {
		const { id, value } = Object.assign({}, this.state, this.props)
		let actualItems
		let validationState

		if (value.includes(ev.target.value)) {
			actualItems = value.filter(el => el !== ev.target.value)	
			validationState = this.validateField(actualItems)

			this.props.onChangeFieldValue(
				id,
				actualItems,
				validationState
			)
		} else {
			actualItems = [...value, ev.target.value]
			validationState = this.validateField(actualItems)

			this.props.onChangeFieldValue(
				id,
				actualItems,
				validationState
			)
		}
	}

	renderOptions = () => {
		const { options, value } = Object.assign({}, this.state, this.props)

		return options.map((opt, i) => {
			return (
				<div className="form-check" key={opt + ' ' + i}>
					<input type="checkbox" className="form-check-input"
						onChange={this.handleChange}
						value={opt}
						checked={value.includes(opt)}
						/>
					<label className="form-check-label">{opt}</label>
				</div>
			)
		})
	}

	render () {
		const { id, label } = Object.assign({}, this.state, this.props)

		return (
			<div id={id} className="form-group">
				<label>{label}</label>	
				{ this.renderOptions() }
			</div>
		)
	}

}

const mapStateToProps = (state, ownProps) => ({
	...findFieldInState(ownProps.id, state)	
})

const mapDispatchToProps = (dispatch) => ({
		onChangeFieldValue: (id, value, validationState) => 
			dispatch(actions.onChangeFieldValue(id, value, validationState))
})

export const ConnChecklistVerticalField = connect(mapStateToProps, mapDispatchToProps)(ChecklistVerticalField)
