import React, { Component } from 'react'
import Datetime from 'react-datetime'
import './DateField.css'

import { connect } from 'react-redux'
import { findFieldInState } from '../../selectors/RecruitmentSurveySelectors'
import * as actions from '../../actions/RecruitmentSurveyActions'

import { validate, tryCatch } from '../../utils/validation'

export default class DateField extends Component {
	constructor (props) {
		super(props)

		this.state = {
			id: "default_id",
			__typename: "default_typename",
			label: "default_label",
			value: "99/99/9999",
			validationState: {
				status: '',
				message: []
			}
		}

		this.handleChange = this.handleChange.bind(this)
		this.renderInput = this.renderInput.bind(this)
		this.validateField = this.validateField.bind(this)
	}

	validateField (value) {
		const errors = validate([
			{ property: "Start Date", rule_name: "isValidDate" }
		])({ "Start Date" : value })

		return {
			status: (errors.length > 0) ? 'is-invalid' : 'is-valid',
			message: errors
		}
	}

	handleChange  (value) {
		const { id } = Object.assign({}, this.state, this.props)
		let date_value = tryCatch(() => value.format('DD/MM/YYYY'),
								  () => String(value))

		const validationState = this.validateField(date_value)

		this.props.onChangeFieldValue(id, date_value, validationState)
	}

	renderInput (props, openCalendar, closeCalendar) {
		const { id, value, label, validationState } = Object.assign({}, this.state, this.props)

		return (
			<InputWrapper label={label}>
				<input 
					{...props}
					value={value}
					id={id}
					className={`form-control ${validationState.status}`}
					type="text"
					placeholder={`99/99/9999`}
					/>
			</InputWrapper>
		)
	}

	render () {
		return (
			<Datetime
				onChange={this.handleChange.bind(this)}
				dateFormat="DD/MM/YYYY"
				timeFormat={false}
				renderInput={this.renderInput.bind(this)}
				/>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
	...findFieldInState(ownProps.id, state)	
})

const mapDispatchToProps = (dispatch) => ({
		onChangeFieldValue: (id, value, validationState) => 
			dispatch(actions.onChangeFieldValue(id, value, validationState))
})

export const ConnDateField = connect(mapStateToProps, mapDispatchToProps)(DateField)

const InputWrapper = (props) =>
	<div className="col-sm-12 col-md-6 m-0 p-0">
		<div className="form-group">
			<label>{props.label}</label>	
			<div className="input-group mb-3">
			{props.children}	

			<div className="input-group-append">
				<span className="input-group-text">
					<i className="far fa-calendar-alt fa-lg"></i>
				</span>
			</div>

			<div className="valid-feedback">{'Valid date'}</div>
			<div className="invalid-feedback">{'Invalid date'}</div>

			</div>
		</div>	
	</div>


