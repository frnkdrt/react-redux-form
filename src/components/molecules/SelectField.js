import React, { Component } from 'react'
import { connect } from 'react-redux'
import { findFieldInState } from '../../selectors/RecruitmentSurveySelectors'
import * as actions from '../../actions/RecruitmentSurveyActions'
import { validate } from '../../utils/validation'

export default class SelectField extends Component {
	constructor (props) {
		super(props)

		this.state = {
			id: "default_id",
			__typename: "default_typename",
			label: "default label",
			options: ["one", "two"],
			value: "",
			validationState : {
				status: '',
				message: []
			}
		}
		this.validateField = this.validateField.bind(this)
		this.handleChange = this.handleChange.bind(this)
		this.renderOptions = this.renderOptions.bind(this)
	}

	validateField (value) {
		const errors = validate([
			{ property: "Primary Skill", rule_name: "isRequired" }
		])({ "Primary Skill" : value })

		return {
			status: errors.length > 0 ? 'is-valid' : 'is-invalid',
			message : errors
		}
	}

	handleChange = (ev) => {
		const { id } = Object.assign({}, this.state, this.props)
		const validationState = this.validateField(ev.target.value)

		this.props.onChangeFieldValue(id, ev.target.value, validationState)
	}

	renderOptions = () => {
		const { options, value } = Object.assign({}, this.state, this.props)

		return ['', ...options].map((opt, i) => {
			return (
				<option key={opt + " " + i}
					value={opt}
					checked={opt === value}
					>
					{ ` ${opt} `}
				</option>
			)
		})
	}

	render () {
		const { id, label } = Object.assign({}, this.state, this.props)
		return (
			<div id={id} className="form-group">
				<label>{label}</label>	
				<select className="form-control"
					onChange={this.handleChange.bind(this)}	
					>
					{ this.renderOptions() }
				</select>	
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
	...findFieldInState(ownProps.id, state)	
})

const mapDispatchToProps = (dispatch) => ({
		onChangeFieldValue: (id, value, validationState) => 
			dispatch(actions.onChangeFieldValue(id, value, validationState))
})

export const ConnSelectField = connect(mapStateToProps, mapDispatchToProps)(SelectField)
