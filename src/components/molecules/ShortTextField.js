import React, { Component } from 'react'
import { connect } from 'react-redux'
import { findFieldInState } from '../../selectors/RecruitmentSurveySelectors'
import * as actions from '../../actions/RecruitmentSurveyActions'
import { validate } from '../../utils/validation'

export default class ShortTextField extends Component {
	constructor (props) {
		super(props)

		this.state = {
			id: "default_id",
			label: "default_label",
			__typename: "default_typename",
			value: "",
			validationState: {
				status: '',
				message: []
			}
		}

		this.handleChange = this.handleChange.bind(this)
		this.validateField = this.validateField.bind(this)
	}

	validateField (value) {
		const errors = validate([
			{ property: "Your Name", rule_name: "minLength", minLength: 10 },
			{ property: "Your Name", rule_name: "maxLength", maxLength: 20 },
			{ property: "Your Name", rule_name: "isValidString", regexp: "a-zA-ZÀ-ú " },
			{ property: "Your Name", rule_name: "optional"}
		])({ "Your Name" : value })

		return errors
	}

	handleChange  (ev) {
		const { id } = Object.assign({}, this.state, this.props)

		const errors = this.validateField(ev.target.value)
		const validationState = {
			status: (errors.length > 0) ? 'is-invalid' : 'is-valid',
			message: errors
		}

		this.props.onChangeFieldValue(id, ev.target.value, validationState)
	}

	render () {
		const { id, label, value, validationState } = Object.assign({}, this.state, this.props)

		return (
			<div id={id} className="form-group">
				<label>{label}</label>	
				<input className={`form-control ${validationState.status}`}
					type="text"
					onChange={this.handleChange.bind(this)}
					value={value}
					/>
				<div className="valid-feedback">{validationState.message.join(' and ')}</div>
				<div className="invalid-feedback">{validationState.message.join(' and ')}</div>
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
	...findFieldInState(ownProps.id, state)	
})

const mapDispatchToProps = (dispatch) => ({
		onChangeFieldValue: (id, value, validationState) => 
			dispatch(actions.onChangeFieldValue(id, value, validationState))
})

export const ConnShortTextField = connect(mapStateToProps, mapDispatchToProps)(ShortTextField)