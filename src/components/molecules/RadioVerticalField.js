import React, { Component } from 'react'
import { connect } from 'react-redux'
import { findFieldInState } from '../../selectors/RecruitmentSurveySelectors'
import * as actions from '../../actions/RecruitmentSurveyActions'
import { validate } from '../../utils/validation'

export default class RadioVerticalField extends Component {
	constructor (props) {
		super(props)

		this.state = {
			id: "default_id",
			label: "default_label",
			__typename: "default_typename",
			options: ["one", "two"],
			value: ""
		}

		this.handleChange = this.handleChange.bind(this)
		this.validateField = this.validateField.bind(this)
		this.renderOptions = this.renderOptions.bind(this)
	}

	validateField (value) {
		const errors = validate([
			{ property: "Javascript library of choice", rule_name: "isRequired" }
		])({ "Javascript library of choice" : value })

		return {
			status: errors.length > 0 ? 'is-valid' : 'is-invalid',
			message : errors
		}
	}

	handleChange (ev) {
		const { id } = Object.assign({}, this.state, this.props)

		const validationState = this.validateField(ev.target.value)
		this.props.onChangeFieldValue(id, ev.target.value, validationState)
	}

	renderOptions () {
		const { id, options, value } = Object.assign({}, this.state, this.props)

		return options.map((opt, i) => {
			return (
				<div className="form-check"
					key={opt + ' ' + i}
					>
					<input className="form-check-input" type="radio"
						name={id}
						onChange={this.handleChange.bind(this)}
						checked={opt === value}
						value={opt}
						/>
					<label className="form-check-label">
						{ ` ${opt} ` }	
					</label>
				</div>
			)
		})
	}

	render () {
		const { id, label } = Object.assign({}, this.state, this.props)

		return (
			<div id={id} className="form-group">
				<label>{label}</label>	
				{ this.renderOptions() }
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
	...findFieldInState(ownProps.id, state)	
})

const mapDispatchToProps = (dispatch) => ({
		onChangeFieldValue: (id, value, validationState) => 
			dispatch(actions.onChangeFieldValue(id, value, validationState))
})

export const ConnRadioVerticalField = connect(mapStateToProps, mapDispatchToProps)(RadioVerticalField)
