import React, { Component } from 'react'
import ButtonPrimary from '../atoms/ButtonPrimary'
import { connect } from 'react-redux'
import * as actions from '../../actions/RecruitmentSurveyActions'

export default class Submit extends Component {
	constructor (props) {
		super(props)

		this.state = {
			header: "Please, verify the following fields: ",
			showError: false,
			invalidFields : []
		}

		this.handleClick = this.handleClick.bind(this)
		this.isFormValid = this.isFormValid.bind(this)
		this.showError = this.showError.bind(this)
		this.handleBlur = this.handleBlur.bind(this)
	}

	isFormValid () {
		const invalidFields = this.props.publicForm.formFields.reduce((acc, field) => {
			return field.validationState.message.length > 0 //|| !(field.value.length > 0)
				? acc.concat({ id: field.id })
				: acc
		}, [])

		if (invalidFields.length > 0) {
			this.setState({
				...this.state,
				showError: true,
				invalidFields : invalidFields
			})
			return false
		} else {
			return true
		}
	}

	handleClick () {
		this.props.onFormSubmit()

		if( this.isFormValid() ) {
			// send fields to dispatch the form
			this.props.dispatchForm(this.props.publicForm.formFields)
		} 
	}

	showError () {
		if (this.props.submit.submitting && this.state.showError) {
			return (
				<div 
					className={`bs-popover-right popover`}
					style={{position: "absolute", left: '16%'}}
					role="tooltip"
				>
					<div className="arrow"></div>
					<div className="popover-header">{this.state.header}</div>
					<div className="popover-body text-danger">{this.state.invalidFields.map(field => field.id).join(', ')}</div>
				</div>
			)
		}
	}

	handleBlur () { this.setState({ showError: false }) }

	render () {
		return (
			<div style={{position: 'relative'}}>
				<ButtonPrimary
					{...this.props}
					type="submit"
					onClick={this.handleClick}
					onBlur={this.handleBlur.bind(this)}
					>
					{this.props.children}
				</ButtonPrimary>
				{ this.showError() }
			</div>
		)

	}
}

const mapStateToProps = (state) => ({
	...state.rootState
})

const mapDispatchToProps = (dispatch) => ({
	onFormSubmit: () => dispatch(actions.onFormSubmit()),
	dispatchForm: (data) => dispatch(actions.dispatchForm(data))	
})

export const ConnSubmit = connect(mapStateToProps, mapDispatchToProps)(Submit)


