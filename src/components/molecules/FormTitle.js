import React from 'react'
import * as c from '../misc/colors'
import styled from 'styled-components'
import { connect } from 'react-redux'
import * as actions from '../../actions/RecruitmentSurveyActions'

export const FormTitle = (props) => {
	/* Defaults */
	const {
		title="Default title",
		organizationName="Default Organization Name"
	} = props

	return (
		<Header>
			<h5 className="text-left">{title}</h5>
			<h5 className="text-left">{organizationName}</h5>
		</Header>
	)
}

const mapStateToProps = (state) => ({
	...state.rootState.publicForm.publicFormSettings
})

const mapDispatchToProps = (dispatch) => ({
	onFieldChange: (patch_object) => dispatch(actions.onChangeFieldValue(patch_object))
})

export const ConnFormTitle = connect(mapStateToProps, mapDispatchToProps)(FormTitle)

const Header = styled.div`
	background-color: ${c.background_01};
	border-radius: 5px;

	> h5 {
		color: ${c.secondary};

		font-family: 'Ubuntu';
		font-weight: 700;
	}

	> h5:nth-child(2) {
		color: ${c.primary}
	}
`