import React, { Component } from 'react'
import { connect } from 'react-redux'

import styled from 'styled-components'
import * as c from '../misc/colors'

import Submit from '../molecules/Submit'
import * as actions from '../../actions/RecruitmentSurveyActions'

import { ConnFormTitle } from '../molecules/FormTitle'
import { ConnShortTextField } from '../molecules/ShortTextField'
import { ConnLongTextField } from '../molecules/LongTextField'
import { ConnSelectField } from '../molecules/SelectField'
import { ConnRadioVerticalField } from '../molecules/RadioVerticalField'
import { ConnChecklistVerticalField } from '../molecules/ChecklistVerticalField'
import { ConnDateField } from '../molecules/DateField'
import { ConnSubmit } from '../molecules/Submit'


class RecruitmentSurvey extends Component {
	componentWillMount () {
		this.props.fetch()
	}

	render () {
		return (
			<Container className="container-fluid p-4 h-100">
				<div className="row">
					<div className="col xs-12 col-md-4">
						<SubContainer className="py-5 px-4 rounded">
							<ConnFormTitle/>
						</SubContainer>
					</div>
					<div className="col xs-12 col-md-8">
						<SubContainer className="py-5 px-5 rounded">
							<ConnShortTextField id="your_name" />					
							<ConnLongTextField id="your_bio"/>					
							<ConnSelectField id="primary_skill"/>
							<ConnRadioVerticalField id="javascript_library_of_choice" />
							<ConnChecklistVerticalField id="additional_experience" />
							<ConnDateField id="start_date" />
							<ConnSubmit />
						</SubContainer>
					</div>
				</div>
			</Container>
		)
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		fetch: () => dispatch(actions.fetchForm())
	}
}

export default connect(
	undefined,
	mapDispatchToProps
)(RecruitmentSurvey)

const Container = styled.div`
	background-color: ${c.background_02}
`

const SubContainer = styled.div`
	background-color: ${c.background_01}
`




