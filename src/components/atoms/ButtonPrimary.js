import React from 'react'
import styled from 'styled-components'
import * as c from '../misc/colors'

export default (props) => {
	/* Defaults */
	const {
		children="Submit"
	} = props

	return (
		<Wrapper className="btn px-4"
			{...props}
			>
			{children}
		</Wrapper>
	)
}

const Wrapper = styled.button`
	color: #FFFFFF;
	background-color : ${c.secondary};

	border-radius: 3px;
`
