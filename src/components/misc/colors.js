export const primary = '#384E63'
export const secondary = '#058bff'
export const background_01 = '#FFFFFF'
export const background_02 = '#E9EFF6'