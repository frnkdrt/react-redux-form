import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as actions from '../RecruitmentSurveyActions'
import * as types from '../types'
import { INITIAL_STATE } from '../../reducers/RecruitmentSurveyReducer'
import moxios from 'moxios'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('RecruitmentSurveyActions fetchFields', () => {
	beforeEach(() => { moxios.install() })
	afterEach(() => { moxios.install() })

	const expectedErr = { message : 'Error message' }
	const expectedObject = {
		publicForm: {
			formFields: [
				{ id: "your_name", label: "Your name", __typename: "ShortTextField" },
				{ id: "your_bio", label: "Your Bio", __typename: "LongTextField"}
			],
			publicFormSettings: {
				organizationName: "Pipefy Recruitment Test",
				submitButtonText: "Submit",
				title: "Recruitment Survey"
			}
		}
	}

	it('creates actions REQUEST and SUCCESS case request succeed', () => {
		const store = mockStore(INITIAL_STATE)

		const expectedActions = [
			{ type: types.RECRUITMENT_SURVEY_FETCH_FORM },
			{ type: types.RECRUITMENT_SURVEY_FETCH_FORM_SUCCESS, payload: expectedObject },
			{ type: types.RECRUITMENT_SURVEY_APPEND_START_VALUE_TO_FIELDS }
		]

		moxios.wait(() => {
			const request = moxios.requests.mostRecent()
			request.respondWith({
				status: 200,
				response: expectedObject
			})
		})

		return store.dispatch(actions.fetchForm()).then(() => {
			expect(store.getActions()).toEqual(expectedActions)
		})
	})

	it('creates actions REQUEST and FAILURE case request fails', () => {
		const store = mockStore(INITIAL_STATE)

		const expectActions = [
			{ type: types.RECRUITMENT_SURVEY_FETCH_FORM },
			{ type: types.RECRUITMENT_SURVEY_FETCH_FORM_FAILURE, payload: expectedErr }
		]

		moxios.wait(() => {
			const request = moxios.requests.mostRecent()
			request.reject(expectedErr)
		})

		return store.dispatch(actions.fetchForm()).then(() => {
			expect(store.getActions()).toEqual(expectActions)
		})
	})
})