import axios from 'axios'
import * as types from './types'
import { formQuery } from '../data/recruitmentSurveyQueries'
import { createMutationObject } from '../data/recruitmentSurveyMutations'
import { pipefyGql, pipefyJson } from '../data/reqConfig'

const fetchingForm = () => ({
	type: types.RECRUITMENT_SURVEY_FETCH_FORM
})

const fetchFormSuccess = data => ({
	type: types.RECRUITMENT_SURVEY_FETCH_FORM_SUCCESS,
	payload: data
})

const fetchFormFailure = err => ({
	type: types.RECRUITMENT_SURVEY_FETCH_FORM_FAILURE,
	payload: err
})

const appendInitialValueToFields = () => ({
	type: types.RECRUITMENT_SURVEY_APPEND_START_VALUE_TO_FIELDS
})

export const fetchForm = () => {
	return dispatch => {
		dispatch(fetchingForm())
		
		return axios(pipefyJson(formQuery))
		.then(res => {
			dispatch(fetchFormSuccess(res.data.data))
			dispatch(appendInitialValueToFields())
		})
		.catch(err => dispatch(fetchFormFailure(err)))
	}
}

export const onChangeFieldValue = (id, value, validationState) => {
	return {	
		type: types.RECRUITMENT_SURVEY_ON_CHANGE_FIELD_VALUE,
		patch: { id, value, validationState }
	}
}

export const onFormSubmit = () => ({
	type: types.RECRUITMENT_SURVEY_ON_FORM_SUBMIT
})

const dispatchingForm = () => ({
	type: types.RECRUITMENT_SURVEY_DISPATCH_FORM
})

export const dispatchFormSuccess = (data) => ({
	type: types.RECRUITMENT_SURVEY_DISPATCH_FORM_SUCCESS,
	payload: data
})

export const dispatchFormFailure = (err) => ({
	type: types.RECRUITMENT_SURVEY_DISPATCH_FORM_FAILURE,
	payload: err
})

export const dispatchForm = (fields) => {
	const mutation = createMutationObject(fields)

	return dispatch => {
		dispatch(dispatchingForm())
		
		return axios(pipefyJson(createMutationObject(fields)))
		.then(res => {
			return {
				type: types.RECRUITMENT_SURVEY_DISPATCH_FORM
			}
		})
		.catch(err => {
			return {
				type: types.RECRUITMENT_SURVEY_DISPATCH_FORM
			}
		})
	}
}










