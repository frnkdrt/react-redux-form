import moment from 'moment'
import axios from 'axios'

const numIterator = (start, end, fn) => {
	const res = []
	for (let i=start||0; i<end||0; i++) {
		res.push(fn(i))
	}
	return res
}

export const tryCatch = (fn, orElse) => {
	try {
		return fn()
	} catch(e) {
		return orElse()
	}
}

const expandRegex = regexp =>
	regexp.match(/.-.|./g)
	.map(exp => exp.length === 3
		? numIterator(
			exp.charCodeAt(0), 
			exp.charCodeAt(2),
			num => String.fromCharCode(num)
		).join('')
		: [exp]
).join('')

const validators = {
	isRequired: (obj, { property }) => {
		return !obj[property].length > 0
			? [`${property} field is required`]
			: []
	},

	isValidString: (obj, { property, regexp }) =>
		!RegExp(`^[${regexp}]+$`).test(obj[property])
			? [`${property} must contain just the chars: ${expandRegex(regexp).slice(0,50).concat('...')}`]
			: [],

	minLength: (obj, { property, minLength }) => 
		!(obj[property].length > minLength)
			? [`${property} must be at last ${minLength} long`]
			: [],

	maxLength: (obj, { property, maxLength }) =>
		!(obj[property].length < maxLength)
			? [`${property} must be at most ${maxLength} long`]
			: [],

	isValidDate: (obj, { property }) =>
		!obj[property].match(/^\d\d\/\d\d\/\d\d\d\d$/) 
			? [`${property} must be in format DD/MM/YYYY`]
			: [],

	isValidMomentDate: (obj, { property }) =>
		!tryCatch(() => obj[property].isValid(),
				  () => false)
			? [`${property} must be a valid date`]
			: [],

	optional: (obj, rule) => []
}

export const validate = (rules) => (obj) => {
	const isOptional = rules.some(rule => rule.rule_name === "optional")
	return rules.reduce((acc, rule) => {
		return isOptional && obj[rule.property].length === 0
			? []
			: acc.concat(validators[rule.rule_name](obj, rule))
	} , [])
}

window.ax = axios