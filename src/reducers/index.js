import { combineReducers } from 'redux'
import RecruitmentSurveyReducer from './RecruitmentSurveyReducer'

export  default combineReducers({
	rootState : RecruitmentSurveyReducer
})