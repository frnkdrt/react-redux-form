import * as types from '../actions/types'

export const INITIAL_STATE = {
	request: {
		fetching: false,
		error: {}
	},
	submit: {
		submitting: false,
		error: {}
	},
	publicForm: {
		formFields: [],
		publicFormSettings: {
			title: 'Recruitment Survey',
			submitting: false
		}
	}
}

export default (state=INITIAL_STATE, action) => {
	switch (action.type) {
		case types.RECRUITMENT_SURVEY_FETCH_FORM: {

			return {
				...state,
				request: {
					...state.request,
					fetching: true
				}
			}
		}

		case types.RECRUITMENT_SURVEY_FETCH_FORM_SUCCESS: {
			const publicForm = action.payload.publicForm

			return {
				...state,
				request: { 
					...state.request,
					fetching: false
				},
				publicForm: {
					...publicForm,
					formFields: publicForm.formFields,
					publicFormSettings: {
						...publicForm.publicFormSettings,
						organizationName: publicForm.publicFormSettings.organizationName,
						submitButtonText: publicForm.publicFormSettings.submitButtonText
					}
				}
			}
		}

		case types.RECRUITMENT_SURVEY_FETCH_FORM_FAILURE: {
			return {
				...state,
				request: {
					...state.request,
					fetching: false,
					error: action.payload
				}
			}
		}

		case types.RECRUITMENT_SURVEY_APPEND_START_VALUE_TO_FIELDS: {
			const formFields = state.publicForm.formFields

			const mapping = {
				ShortTextField: "",
				LongTextField: "",
				SelectField: "",
				RadioVerticalField: "",
				ChecklistVerticalField: [],
				DateField: ""
			}

			return {
				...state,
				publicForm: {
					...state.publicForm,
					formFields: formFields.map(field => Object.assign({}, field, {
						value: mapping[field.__typename],
						validationState: {
							status: '',
							message: []
						}
					}))
				}
			}
		}
			
		case types.RECRUITMENT_SURVEY_ON_CHANGE_FIELD_VALUE: {
			const formFields = state.publicForm.formFields

			return {
				...state,
				publicForm: {
					...state.publicForm,
					formFields: formFields.map(field => 
						field.id === action.patch.id 
							? Object.assign(
								{}, 
								field,
								{ 
									value: action.patch.value ,
									validationState: action.patch.validationState
								}
							) 
							: field
					)
				}
			}
		}

		case types.RECRUITMENT_SURVEY_ON_FORM_SUBMIT:
			return {
				...state,
				submit: {
					...state.submit,
					submitting: true
				}
			}

		case types.RECRUITMENT_SURVEY_DISPATCH_FORM:
			return {
				...state,
				submit: {
					...state.submit,
					submitting: false,
					dispatch: true,
				}
			}

		case types.RECRUITMENT_SURVEY_DISPATCH_FORM_SUCCESS: 
			return {
				...state,
				submit: {
					...state.submit,
					dispatch: false,
					data: action.data
				}
			}

		case types.RECRUITMENT_SURVEY_DISPATCH_FORM_FAILURE: 
			return {
				...state,
				submit: {
					...state.submit,
					dispatch: false,
					error: action.err
				}
			}

		default: 
			return state
	}
}