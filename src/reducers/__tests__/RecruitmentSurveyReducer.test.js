import * as types from '../../actions/types'
import reducer from '../RecruitmentSurveyReducer'
import { INITIAL_STATE } from '../RecruitmentSurveyReducer'


describe('RecruitmentSurveyReducer Fetching Form', () => {
	const mockPayload = {
		publicForm: {
			formFields: [
				{ id: "your_name", label: "Your name", __typename: "ShortTextField" },
				{ id: "your_bio", label: "Your Bio", __typename: "LongTextField"}
			],
			publicFormSettings: {
				organizationName: "Pipefy Recruitment Test",
				submitButtonText: "Submit",
				title: "Recruitment Survey"
			}
		}
	}

	it('shoud return default state', () => {
		expect(reducer(undefined, {})).toEqual(INITIAL_STATE)
	})

	it('should set `request.fetching` to true on FETCH_FORM_REQUEST', () => {
		const mockAction = { type: types.RECRUITMENT_SURVEY_FETCH_FORM }

		expect(reducer(INITIAL_STATE, mockAction))
		.toEqual({
			...INITIAL_STATE,
			request: {
				...INITIAL_STATE.request,
				fetching: true
			}
		})
	})

	it('should set `request.fetching` to `false` on FETCH_FORM_SUCCESS \
		OR FETCH_FORM_FAILURE', () => {
		let mockActions = [
			{ type: types.RECRUITMENT_SURVEY_FETCH_FORM },
			{ type: types.RECRUITMENT_SURVEY_FETCH_FORM_SUCCESS, mockPayload }
		]

		expect(reducer(INITIAL_STATE, mockActions).request.fetching)
		.toEqual(false)

		mockActions = [
			{ type: types.RECRUITMENT_SURVEY_FETCH_FORM },
			{ type: types.RECRUITMENT_SURVEY_FETCH_FORM_FAILURE, mockPayload }
		]

		expect(reducer(INITIAL_STATE, mockActions).request.fetching)
		.toEqual(false)
	})

	it('should return new state after FETCH_FORM_SUCCESS', () => {
		const mockAction = {
			type: types.RECRUITMENT_SURVEY_FETCH_FORM_SUCCESS,
			payload: mockPayload
		}

		expect(reducer(INITIAL_STATE, mockAction))
		.toEqual({
			request: {
				...INITIAL_STATE.request,
				fetching: false,
			},
			publicForm: {
				formFields : mockPayload.publicForm.formFields,
				publicFormSettings : mockPayload.publicForm.publicFormSettings
			}
		})
	})
})

describe('RecruitmentSurveyReducer Mutating', () => {
	const mockInitialState = {
		...INITIAL_STATE,
		publicForm: {
			...INITIAL_STATE.publicForm,
			formFields : [
				{ id: "your_name", label: "Your name", __typename: "ShortTextField" },
				{ id: "additional_experience", label: "Additional Expeirence", 
				  __typename: "ChecklistVerticalField", 
				  options: [ "TDD", "Heroku", "Github"] },
				{ id: "javascript_library_of_choice", label: "Javascript library of choice",
				  __typename: "RadioVerticalField",
				  options: [ "React", "Angular", "Vue" ] }
			]
		}
	}

	it('should append empty values to `publicForm.formFields` accordinly with \
		`__typename` of fields', () => {
		const mockAction = {
			type: types.RECRUITMENT_SURVEY_APPEND_START_VALUE_TO_FIELDS,
		}

		expect(reducer(mockInitialState, mockAction))
		.toEqual({
			...mockInitialState,
			publicForm: {
				...mockInitialState.publicForm,
				formFields: [
					{ id: "your_name", label: "Your name", __typename: "ShortTextField", value: "" },
					{ id: "additional_experience", label: "Additional Expeirence", 
					__typename: "ChecklistVerticalField", 
					options: [ "TDD", "Heroku", "Github"], value: [] },
					{ id: "javascript_library_of_choice", label: "Javascript library of choice",
					__typename: "RadioVerticalField",
					options: [ "React", "Angular", "Vue" ], value: "" }
				]
			}
		})
	})

	it('should change value correctly on `CHANGE_FIELD_VALUE`', () => {
		let mockAction = {
			type: types.RECRUITMENT_SURVEY_ON_CHANGE_FIELD_VALUE,
			patch: {
				id: "your_name",
				value: "I'm writ..."
			}
		}

		expect(reducer(mockInitialState, mockAction).publicForm.formFields)
		.toEqual(
			mockInitialState.publicForm.formFields.map(field =>
				field.id === mockAction.patch.id 
					? Object.assign({}, field, { value: "I'm writ..."})
					: field
		))
	})

})
