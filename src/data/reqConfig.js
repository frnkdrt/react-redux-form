export const pipefyGql = query => ({
	url: 'http://app.pipefy.com/public_api',
	method: 'post',
	headers: {'Content-Type': 'application/graphql'},
	// query: query,
	data: `query ${ query }`
})

export const pipefyJson = (query) => ({
	url: 'https://app.pipefy.com/public_api',
	method: 'post',
	headers: {
		'Content-Type': 'application/json',
		'Accept': 'application/json'
	},

	data: {
		query:  query,
	}
})