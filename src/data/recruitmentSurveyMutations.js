export const formMutation = (input) => 
	`mutation {
		submitPublicForm(input: {
			formId: "1lf_E0x4",
			filledFields: ${input}
			}) {
			repoItem {
				id
				title
			}
			}
		}`

export const createMutationObject = (fields) => {
	let variables = fields.reduce((acc, field) => 
		acc.concat(
			{  	
				fieldId : field.id,
				fieldValue: String(field.value) 
			}
		), [])

	// remove quotes from object key
	variables = JSON.stringify(variables).replace(/\"([^(\")"]+)\":/g,"$1:")

	return formMutation(variables)
}



