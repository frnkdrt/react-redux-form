import React, { Component } from 'react';
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import reducers from './reducers'

import RecruitmentSurvey from './components/pages/RecruitmentSurvey'
import * as actions from './actions/RecruitmentSurveyActions'

const store = createStore(
  reducers,
  applyMiddleware(thunk)
)

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <RecruitmentSurvey/>
      </Provider>
    );
  }
}

export default App;